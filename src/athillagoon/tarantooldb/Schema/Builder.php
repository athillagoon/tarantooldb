<?php namespace Athillagoon\Tarantooldb\Schema;

use Closure;
use Athillagoon\Tarantooldb\Connection;

class Builder extends \Illuminate\Database\Schema\Builder
{
    /**
     * @inheritdoc
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @inheritdoc
     */
    public function hasColumn($table, $column)
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function hasColumns($table, array $columns)
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public function hasTable($collection)
    {
        return $this->collection->hasSpace($collection);
        // return $this->connection->call('has_space', $collection);
    }

    /**
     * Modify a collection on the schema.
     *
     * @param  string  $collection
     * @param  Closure $callback
     * @return bool
     */
    public function collection($collection, Closure $callback)
    {
        $blueprint = $this->createBlueprint($collection);

        if ($callback) {
            $callback($blueprint);
        }
    }

    /**
     * @inheritdoc
     */
    public function table($collection, Closure $callback)
    {
        return $this->collection($collection, $callback);
    }

    /**
     * @inheritdoc
     */
    public function create($collection, Closure $callback = null)
    {           
        $fluent_columns = tap($this->createBlueprint($collection), function ($blueprint) use ($callback) {
            $blueprint->create();
            if ($callback) {
                $callback($blueprint);
            }
        })->getColumns();

        $this->connection->createSpace(['table' => $collection, 'fields' => $this->fluentArrayToArray($fluent_columns)]);
    }

    protected function fluentArrayToArray($fluent_array)
    {
        $new_array = [];

        foreach ($fluent_array as $value) {
            $new_array[] = $value->toArray();
        }

        return $new_array;
    }

    /**
     * @inheritdoc
     */
    public function drop($collection)
    {
        $blueprint = $this->createBlueprint($collection);

        return $blueprint->drop();
    }

    /**
     * @inheritdoc
     */
    protected function createBlueprint($collection, Closure $callback = null)
    {        
        return new Blueprint($this->connection, $collection);
    }
}
