<?php namespace Athillagoon\Tarantooldb;

use Illuminate\Database\Connection as BaseConnection;
use Tarantool as TarantoolDriver;

class Connection extends BaseConnection
{    

    /**
     * The Tarantool connection handler.
     *     
     */
    protected $connection;

    /**
     * Create a new database connection instance.
     *
     * @param  array $config
     */
    public function __construct(array $config)
    {
        // if (extension_loaded('tarantool')) 
        //     throw new \Exception('Tarantool extension for php not found!' . (file_exists('/usr/lib/php/20151012/sockets.so') ? 1 : 2) . ';');

        $this->config = $config;
        
        $options = array_get($config, 'options', []);

        // Create the connection
        $this->connection = $this->createConnection($config, $options);


        $this->useDefaultPostProcessor();

        $this->useDefaultSchemaGrammar();
    }

    /**
     * Begin a fluent query against a database collection.
     *
     * @param  string $collection
     * @return Query\Builder
     */
    public function collection($collection)
    {
        $query = new Query\Builder($this, $this->getPostProcessor());

        return $query->from($collection);
    }
    

    /**
     * Begin a fluent query against a database collection.
     *
     * @param  string $table
     * @return Query\Builder
     */
    public function table($table)
    {
        return $this->collection($table);
    }

    /**
     * Get a MongoDB collection.
     *
     * @param  string $name
     * @return Collection
     */
    public function getCollection($name)
    {
        return new Collection($this, $name);
    }

    /**
     * @inheritdoc
     */
    public function getSchemaBuilder()
    {
        return new Schema\Builder($this);
    }


    /**
     * return Tarantool object.
     *
     * @return \Tarantool
     */
    public function getTarantoolClient()
    {
        return $this->connection;
    }
   

    /**
     * Create a new Tarantool connection.
     *
     * @param  array  $config
     * @param  array  $options
     * @return Tarantool
     */
    protected function createConnection(array $config, array $options)
    {
        // Check if the credentials are not already set in the options
        if (! isset($options['username']) && ! empty($config['username'])) {
            $options['username'] = $config['username'];
        }
        if (! isset($options['password']) && ! empty($config['password'])) {
            $options['password'] = $config['password'];
        }
        if (! isset($options['persistent_id']) && ! empty($config['persistent_id'])) {
            $options['persistent_id'] = $config['persistent_id'];
        }
        

        $user = isset($options['username']) ? $options['username'] : 'guest';
        $password = isset($options['password']) ? $options['password'] : null;
        $persistent_id = isset($options['persistent_id']) ? $options['persistent_id'] : null;
        
        return new TarantoolDriver($config['host'], $config['port'], $user, $password, $persistent_id);
    }

    /**
     * @inheritdoc
     */
    public function disconnect()
    {
        if ($this->connection) {
            $this->connection->disconnect();
        }
    }
    

    /**
     * @inheritdoc
     */
    public function getElapsedTime($start)
    {
        return parent::getElapsedTime($start);
    }

    /**
     * @inheritdoc
     */
    public function getDriverName()
    {
        return 'tarantooldb';
    }

    /**
     * @inheritdoc
     */
    protected function getDefaultPostProcessor()
    {
        return new Query\Processor();
    }

    /**
     * @inheritdoc
     */
    protected function getDefaultQueryGrammar()
    {
        return new Query\Grammar();
    }

    /**
     * @inheritdoc
     */
    protected function getDefaultSchemaGrammar()
    {
        return new Schema\Grammar();
    }

    /**
     * Dynamically pass methods to the connection.
     *
     * @param  string $method
     * @param  array  $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {        
        return call_user_func_array([$this->getCollection(''), $method], $parameters);
    }
}
