<?php namespace Athillagoon\Tarantooldb;

use Exception;

class Collection
{
    /**
     * The connection instance.
     *
     * @var Connection
     */
    protected $connection;    

    protected $name;

    protected $function_map = [
        'createSpace'  => 'create_space',
        'dropSpace'    => 'drop_space',
        'hasSpace'     => 'has_space',
        'createIndex'  => 'create_index',
        'dropIndex'    => 'drop_index',
        'updateMany'   => 'update_stub',
        'insertOne'    => 'insert_stub',
        'insertMany'   => 'insert_stub',
        'deleteOne'    => 'remove_stub',
        'deleteMany'   => 'remove_stub',
        'drop'         => 'truncate_stub',
        'aggregate'    => 'aggregate_stub',
        'distinct'     => 'distinct_stub',
        'find'         => 'find_stub'
    ];

    /**
     * @param Connection      $connection
     */
    public function __construct(Connection $connection, $name)
    {
        $this->connection = $connection;
        $this->name = $name;
    }

    public function getCollectionName()
    {
        return $this->name;
    }

    /**
     * Handle dynamic method calls.
     *
     * @param  string $method
     * @param  array  $parameters
     * @return mixed
     */
    public function __call($method, $parameters)
    {   
        if (strlen($this->name) > 0) {
            $parameters['table'] = $this->name;
        }

        // if ($method == 'insertOne' || $method == 'insertMany') dd($parameters);
        // print($method);        
        // print_r($parameters);

        $result = $this->connection->getTarantoolClient()
                ->call($this->function_map[$method], [$parameters]);

//     TODO: add logging of queries and parameters

        return $result;
    }
}
